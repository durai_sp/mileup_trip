"""
Dashboard.

Author: Durai S
email: dsankaran@agero.com
Date: Jul 11, 2017

Dashboard class has the functionality to initiate mileup_trip.
"""

from fetch_data import FetchData

class Dashboard(object):

    def __init__(self, build, stream_name):
	self.build = build
	self.stream_name = stream_name

    def handler(self):
    	f_data = FetchData(self.build, 'us-east-1', self.stream_name, 'DEBUG')
    	f_data.get_data()


