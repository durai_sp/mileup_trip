"""
FetchData.

Author: Durai S
email: dsankaran@agero.com
Date: Jul 11, 2017

FetchData class has the functionality to fetch trip meta data from DynamoDB and load it in FirehoseStream.
"""

from platformutils.dynamodb_utils import DynamoDBUtils
from platformutils.utils.time_util import TimeUtil
from platformutils.firehose_utils import FirehoseUtils

class FetchData(object):

    def __init__(self, build, region_name, stream_name, level):
	trip_table = 'ppacn-agero-{:}-trip'.format(build)
	self.db = DynamoDBUtils(trip_table, region_name, level)
	self.time =TimeUtil()    
	self.build = build
	self.firehose = FirehoseUtils(stream_name, region_name, level)


    def get_data(self):
	"""
	This method is used to fetch data from DynamoDB.
        """
	utc_value = self.time.get_utc_timestamp(1, 'ms')
	response = self.db.query_table('processedTimeUtc', utc_value, 'processedTimeUtc-index')
	records = []
	batch_size = 0	
	while True:
	    for row in response['Items']:
		handled_d = self.handle_filter_data(row)
		records.append(handled_d)
		batch_size += 1 
		if batch_size == 500:
		    self.put_record(records)
	    	    records = []
	    	    batchSize = 0
	    
	    if response.get('LastEvaluatedKey'):
	        response = self.db.query_table('processedTimeUtc', utc_value, 'processedTimeUtc-index', last_evaluated_key = response['LastEvaluatedKey'])
	    else:
		break
	if batch_size > 0:
	    self.put_record(records)


    def handle_filter_data(self, data):
	"""
	This method is used to handle the data.
        :param data: Provide data which is get from DynamoDB.
        """
	os = data.get('os')
    	deviceModel = data.get('deviceModel')
    	status = int(data.get('status', 0))
    	if os=="ios":
	    if "," in deviceModel:
    	    	a, b = deviceModel.split(",")
	    	deviceModel_ouput_str = a + "#" + b
	    else:
	        deviceModel_ouput_str = deviceModel
	else:
	    deviceModel_ouput_str = deviceModel

	tripId = data.get('tripId')
    	deviceId = data.get('deviceId')
	distance = data.get('distance')
	duration = int(data.get('duration', 0))
	endLat = round(data.get('endLat', 0), 6)
	endLong = round(data.get('endLong', 0), 6)
	hardAccels = int(data.get('hardAccels', 0))
	startLat = round(data.get('startLat', 0), 6)
	startLong = round(data.get('startLong', 0), 6)
	suddenBrakes = int(data.get('suddenBrakes', 0))
	transportMode = int(data.get('transportMode', 0))
    	userId = data.get('userId')
	endUtcDtime = int(data.get('endUtcDtime', 0))
	insertTime = int(data.get('insertTime', 0))
	startUtcDtime = int(data.get('startUtcDtime', 0))
	appVersion = data.get('appVersion')
	appName = data.get('appName')
	accelerationScore = int(data.get('accelerationScore', 0))
	brakingScore = int(data.get('brakingScore', 0))
	speedingScore = int(data.get('speedingScore', 0))
	endBattery = int(data.get('endBattery', 0))
	startBattery = int(data.get('startBattery', 0))
	tripScore = int(data.get('tripScore', 0))
	minutesSpeeding = int(data.get('minutesSpeeding', 0))
	phoneScore = int(data.get('phoneScore', 0))
	insertLocalTime = data.get('insertLocalTime')
	startLocalTime = data.get('startLocalTime')
	endLocalTime = data.get('endLocalTime')
	tripStartReason = data.get('tripStartReason')
	minutesPhone = int(data.get('minutesPhone', 0))
	modifiedBy = int(data.get('modifiedBy', 0))
	insertedBy = int(data.get('insertedBy', 0))
	eventCount = data.get('eventCount')
	modifyLocalTime = data.get('modifyLocalTime')
	groupCode = data.get('groupCode')
	extApp = data.get('extApp')
	reasonCode = data.get('reasonCode')
	modifiedLocalTime = data.get('modifiedLocalTime')
	modifiedTime = int(data.get('modifiedTime', 0))
	modifyTimeN = data.get('modifyTimeN')
	osVersion = data.get('osVersion')
	crashEvents = int(data.get('crashEvents', 0))
	callEvents = int(data.get('callEvents', 0))
	uploadMode = int(data.get('uploadMode', 0))
	sdkVersion = data.get('sdkVersion')
	CS = data.get('CS')
	processedTimeUtc = int(data.get('processedTimeUtc', 0))

	trip_metadata = str(tripId) + "," + str(deviceId) + "," + str(distance) + "," + `duration` + "," + `endLat` + "," + `endLong` + "," + `hardAccels` + "," + str(os) + "," + `startLat` + "," + `startLong` + "," + `status` + "," + `suddenBrakes` + "," + `transportMode` + "," + str(userId) + "," + `endUtcDtime` + "," + `insertTime` + "," + `startUtcDtime` + "," + str(appVersion) + "," + str(appName) + "," + `accelerationScore` + "," + `brakingScore` + "," + `speedingScore` + "," + `endBattery` + "," + `startBattery` + ","  + `tripScore` + "," + `minutesSpeeding` + "," + `phoneScore` + "," + str(insertLocalTime) + "," + str(startLocalTime) + "," + str(endLocalTime) + "," + str(tripStartReason) + "," + `minutesPhone` + "," + `modifiedBy` + "," + `insertedBy` + "," + str(eventCount) + "," + str(modifyLocalTime) + "," + str(groupCode) + "," + str(extApp) + "," + str(reasonCode) + "," + str(modifiedLocalTime) + "," + `modifiedTime` + "," + str(modifyTimeN) + "," + str(osVersion) + "," + `crashEvents` + "," + `callEvents` + ","+ `uploadMode` + "," + str(sdkVersion) + "," + str(deviceModel_ouput_str) + "," + str(CS) + "," + `processedTimeUtc`
	return {'Data':trip_metadata + "\n"}

    def put_record(self, records):
	"""
	This method is used to load records to Firehose Stream.
        :param records: Provide records has to be load.
        """
	self.firehose.put_records(records)
