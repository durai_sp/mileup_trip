"""
SNS connection utils.

Author: Srinivas Rao Cheeti
email: scheeti@agero.com
Date: Mar 28, 2017

SNSUtils class has the necessary functions to publish and read a message to sns topic.
"""

import boto3
import logging_helper as Log


class SNSUtils():
    def __init__(self, r_name, level):
        self.logs = Log.log(level)
        self.logs.info('SNS Utils initialized')
        self.sns_client = boto3.client('sns', region_name=r_name)

    def publish_message(self, arn, message):
        """This method is used to publish a message to SNS topic.

        :param arn: Provide the ARN of the sns topic to publish message.
        :param message: Provide the message to be published to the queue.
        :return This method returns a response message on the details of the published message.
        """
        self.logs.info('Writing this {:} to this {:}'.format(message, arn))
        response = self.sns_client.publish(
            TopicArn=arn,
            Message=message
        )
        return response

    @staticmethod
    def read_message(lambda_event):
        """This method is used to publish a message to SNS topic.

        :param lambda_event: Provide the lambda event from the handler.
        :return This method returns a response message and the topic arn.
        """
        message = ''
        topic_arn = ''
        for records in lambda_event['Records']:
            message = records['Sns']['Message']
            topic_arn = records['Sns']['TopicArn']
        return message, topic_arn
