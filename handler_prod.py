"""
handler.

Author: Durai S
email: dsankaran@agero.com
Date: Jul 11, 2017

handle functionality is used to initiate trip event for prod environment.
"""

from dashboard import Dashboard

def handler():
    dashboard = Dashboard('prod', 'prod-trip-metadata')
    dashboard.handler()

handler()
